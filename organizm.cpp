#pragma once
//using namespace std;
#include "Czlowiek.h"
#include "biblioteka.h"
namespace project{
	Organizm::~Organizm() {

	}
	Organizm::Organizm(Organizm &stary) :
		x(stary.x),
		y(stary.y),
		id(stary.id),
		sila(stary.sila),
		nazwa(stary.nazwa),
		wiek(stary.wiek),
		inicjatywa(stary.inicjatywa),
		pochodzenie(stary.pochodzenie)
	{}
	Organizm::Organizm(int xx, int yy, int idd, int silaa, int inicjatywaa, char *wyraz, int wiekk, Swiat &swiat) :
		x(xx),
		y(yy),
		id(idd),
		sila(silaa),
		nazwa(wyraz),
		wiek(wiekk),
		inicjatywa(inicjatywaa),
		pochodzenie(swiat) {};
	bool Organizm::operator==(Organizm *other) {
		if (this->x == other->x){
			if (this->y == this->y)
				if (this->id == this->id)
					return true;
		}
		else return false;
	}
	int Organizm::getSila(){
		return sila;
	}
	int Organizm::getInicjatywa(){
		return inicjatywa;
	}
	int Organizm::getWiek(){
		return wiek;
	}
	int Organizm::getId(){
		return id;
	}
	int Organizm::getX(){
		return x;
	}
	int Organizm::getY(){
		return y;
	}
	void Organizm::setRuch(int ruch){
		this->ruch = ruch;
	}
	void Organizm::setSila(int x){
		sila = x;
	}
	void Organizm::setWiek(int x){
		wiek = x;
	}
}