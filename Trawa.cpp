#pragma once
//using namespace std;
#include "trawa.h"
#include "biblioteka.h"
namespace project{
	Trawa::Trawa(int xx, int yy, Swiat &swiat)
		:
		Roslina(xx,yy,100,0,0,"trawa",1,swiat){};
	void Trawa::akcja(){
		if (wiek >= 0 && wiek <= 35)
			Roslina::akcja();
		wiek = wiek + 3;
	}
	char Trawa::rysowanie(){
		return 'm';
	}
	Trawa::~Trawa(){
		std::ostringstream inttostring;
		std::string komunikat;
		komunikat += nazwa;
		komunikat += " zjedzona pozycja x ";
		inttostring << x + 1;
		komunikat += inttostring.str();
		inttostring.str("");
		komunikat += ",y ";
		inttostring << y + 1;
		komunikat += inttostring.str();
		inttostring.str("");
		pochodzenie.wypiszkomunikat(komunikat);
	}
}