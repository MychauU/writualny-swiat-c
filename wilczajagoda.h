#pragma once
//using namespace std;
#include "Roslina.h"
namespace project{
	class WilczaJagoda : public Roslina{
	public:
		int kolizja(Zwierze *kolizujacy);
		//void kolizja(Zwierze *kolizujacy);
		char rysowanie();
		void akcja();
		WilczaJagoda(int xx, int yy, Swiat &swiat);
		~WilczaJagoda();
	};
}