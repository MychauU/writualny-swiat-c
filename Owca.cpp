#pragma once
//using namespace std;
#include "Owca.h"
#include "biblioteka.h"
namespace project{
	Owca::Owca(int xx, int yy, Swiat &swiat)
		:
		Zwierze(xx, yy, 201, 4, 4, "owca", 1, swiat){};
	Owca::~Owca(){
		std::ostringstream inttostring;
		std::string komunikat;
		komunikat += nazwa;
		komunikat += " ginie na pozycji x ";
		inttostring << x + 1;
		komunikat += inttostring.str();
		inttostring.str("");
		komunikat += ", y ";
		inttostring << y + 1;
		komunikat += inttostring.str();
		inttostring.str("");
		pochodzenie.wypiszkomunikat(komunikat);
	};
	char Owca::rysowanie(){
		return 'S';
	}
}