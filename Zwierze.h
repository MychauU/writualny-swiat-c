//#ifndef ZWIERZE_H
//#define ZWIERZE_H
#pragma once
//using namespace std;
#include "Organizm.h"
namespace project{
	class Zwierze : public Organizm{
	protected:
		int zmeczony;
	public:
		void addSila(int i);
		Zwierze(int xx, int yy, int id, int sila, int inicjatywa, char *wyraz, int wiek, Swiat &swiat) :
			zmeczony(0),
			Organizm(xx, yy, id, sila, inicjatywa, wyraz, wiek, swiat){}
		//~Zwierze();
		int getZmeczony(){
			return zmeczony;
		}
		void akcja();
		int kolizja(Zwierze *kolizujacy);
	};
}
//#endif