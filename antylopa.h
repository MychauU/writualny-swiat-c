#pragma once
//using namespace std;
#include "Zwierze.h"
namespace project{
	class Antylopa : public Zwierze{
	public:
		char rysowanie();
		void akcja();
		int kolizja(Zwierze *kolizujacy);
		Antylopa(int xx, int yy, Swiat &swiat);
		~Antylopa();
	};
}