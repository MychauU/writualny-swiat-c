#pragma once
//using namespace std;
#include "Guarana.h"
#include "Zwierze.h"
#include "biblioteka.h"
namespace project{
	Guarana::Guarana(int xx, int yy, Swiat &swiat)
		:
		Roslina(xx, yy, 102, 0,0, "guarana", 1, swiat){};
	char Guarana::rysowanie(){
		return '{';
	}
	int Guarana::kolizja(Zwierze *kolizujacy){
		kolizujacy->addSila(3);
		return ZABIJ;
	}
	/*void Guarana::kolizja(Zwierze *kolizujacy){
		kolizujacy->setSila(3);
		delete this;
	}*/
	void Guarana::akcja(){
		if (wiek >= 0 && wiek <= 35)
			Roslina::akcja();
		wiek = wiek + 3;
	}
	Guarana::~Guarana(){}
}