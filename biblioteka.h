//#ifndef BIBLIOTEKA_H
//#define BIBLIOTEKA_H
#pragma once
//using namespace std;
#define MAX_X 28
#define MAX_Y 18
#define MENUX 60
#define MENUY 1
#define STARTX 4
#define STARTY 4
#define PODMENUX 48
#define PODMENUY 19
#include <ctime>
#include <io.h>
#include <fcntl.h>
#include "conio2.h"
#include <time.h>
#include <windows.h>
#include <iostream>
#include <string>
#include <sstream>
namespace project{
	static int komnastepny = 0;

	class Organizm;
	class Swiat{
	public:
		void swappointers(Organizm** a, Organizm** b);
		void wykonajTure();
		void rysujSwiat();
		Organizm** getOrg();
		Organizm** getKol();
		int getXsize();
		int getYsize();
		int getSizeOfHeap();
		Swiat();
		void transferujSwiat(Swiat &stary);
		Swiat(Swiat &stary);
		~Swiat();
		void wypiszkomunikat(std::string &komunikat);
		void czysckomunikat();
	private:
		int max_x;
		int max_y;
		int size_active;
		Organizm **kolejnosc;
		Organizm **organizmy;
		Organizm* seekHuman();
		void ustawSwiat();
		void dodajCzas();
		std::string Swiat::zapisznazwe(std::string &nazwa);
	//	friend void Roslina::akcja();
	//	friend void Zwierze::akcja();
	//	friend void Lis::akcja();
	//	friend void Antylopa::akcja();
	//	friend int Antylopa::kolizja(Zwierze *kolizujacy);
		friend std::ostream& operator<<(std::ostream & left, Swiat& right);
		friend std::istream& operator>>(std::istream & left, Swiat& right);
		void wypisz(std::ostream & out = std::cout);
		void zapisz(std::istream & in = std::cin);
		void rysuj_menu();
		void czyscpodmenu();
		void ustawStos();
		void dodajKrok();
		void rysujMape();
		int losujIndex();
//		void zmiennanull(Organizm *x);
		void heapify(int &i, int &size);
		void heap_build();
		void heap_insert(Organizm *x);
		Organizm* heap_remove();
		void dodajOrganizmy();
		struct t_foot{
			clock_t czas;  //mierzenie czasu
			int kroki;   //mierzenie ilosci wykonanych krokow
		}Czas_Krok;
	};
	enum kierunek_t { /* deklaracja typu enum aby poruszac sie po mapie */
		LEWO =256+ 0x4b, /* pierwszy element ma wartosc  znaku strzalki w lewo */
		PRAWO = 256+0x4d, /* drugi element ma wartosc znaku strzalki w prawo */
		GORA =256+ 0x48, /* itd. */
		DOL = 256+0x50,
		ZABIJ = 1,
		UMIERAJ = 9999,
		ZUM=666,
		ROZMNAZAJ = 9003,
		NIC = 1111,
		UCIEKL=707
	};
	enum tabela_orgranizmow { /* deklaracja typu enum aby poruszac sie po mapie */
		TRAWA=100,
		MLECZ=101,
		GUARANA=102,
		WILCZAJAGODA=103,
		WILK=201,
		OWCA=202,
		LIS=203,
		ZOLW=204,
		ANTYLOPA=205,
	};
}
//#endif
