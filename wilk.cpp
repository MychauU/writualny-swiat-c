#pragma once
//using namespace std;
#include "wilk.h"
#include "biblioteka.h"
namespace project{
	Wilk::Wilk(int xx, int yy, Swiat &swiat)
		:
		Zwierze(xx, yy, 201, 9, 5, "wilk", 1, swiat){};
	Wilk::~Wilk(){
		std::ostringstream inttostring;
		std::string komunikat;
		komunikat += nazwa;
		komunikat += " ginie na pozycji x ";
		inttostring << x + 1;
		komunikat += inttostring.str();
		inttostring.str("");
		komunikat += ", y ";
		inttostring << y + 1;
		komunikat += inttostring.str();
		inttostring.str("");
		pochodzenie.wypiszkomunikat(komunikat);
	};
	char Wilk::rysowanie(){
		return 'W';
	}
}