#pragma once
//using namespace std;
#include "Czlowiek.h"
#include "biblioteka.h"
#define MOC 155056
namespace project{
	Czlowiek::Czlowiek(int xx, int yy, Swiat &swiat)
		:
		moc(0),
		nietykalnosc(false),
		niesmiertelonsc(false),
		szybkosc(0),
		powerbuff(0),
		cooldown(0),
		Zwierze(xx, yy, MOC % 5, 5, 4, "czlowiek", 1, swiat){};
	Czlowiek::~Czlowiek(){};
	char Czlowiek::rysowanie(){
		return 'H';
	}
	void Czlowiek::akcja(){
		int max_x = pochodzenie.getXsize();
		int max_y = pochodzenie.getYsize();
		if (powerbuff) {
			powerbuff--;
			sila--;
		}
		if (cooldown) cooldown--;
		Organizm **organizmy = pochodzenie.getOrg();
		Organizm **kolejnosc = pochodzenie.getKol();
		int size_active = pochodzenie.getSizeOfHeap();
		int zdarzenie;
		if (this->ruch == GORA){
			if (this->y > 0){
				if ((organizmy[x + (y - 1)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y - 1)*max_x]);
					this->y--;
				}
				else {
					zdarzenie = organizmy[x + (y - 1)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y - 1)*max_x]);
						this->y--;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y - 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
						//	pochodzenie.zmiennanull(organizmy[x + (y - 1)*max_x]);
						delete organizmy[x + (y - 1)*max_x];
						organizmy[x + (y - 1)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y - 1)*max_x]);
						this->y--;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y - 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
						//	pochodzenie.zmiennanull(organizmy[x + (y - 1)*max_x]);
						delete organizmy[x + (y - 1)*max_x];
						organizmy[x + (y - 1)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);
					else if (zdarzenie == ROZMNAZAJ);
				}
			}
		}
		else if (ruch == DOL){
			if (this->y < max_y - 1){
				if ((organizmy[x + (y + 1)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y + 1)*max_x]);
					this->y++;
				}
				else {
					zdarzenie = organizmy[x + (y + 1)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y + 1)*max_x]);
						this->y++;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y + 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
						//	pochodzenie.zmiennanull(organizmy[x + (y + 1)*max_x]);
						delete organizmy[x + (y + 1)*max_x];
						organizmy[x + (y + 1)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y + 1)*max_x]);
						this->y++;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y + 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
						//		pochodzenie.zmiennanull(organizmy[x + (y + 1)*max_x]);
						delete organizmy[x + (y + 1)*max_x];
						organizmy[x + (y + 1)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);
					else if (zdarzenie == ROZMNAZAJ);
				}
			}
		}
		else if (ruch == LEWO){
			if (this->x > 0){
				if ((organizmy[x - 1 + (y)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x - 1 + (y)*max_x]);
					this->x--;
				}
				else {
					zdarzenie = organizmy[x - 1 + (y)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x - 1 + (y)*max_x]);
						this->x--;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x - 1 + (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
						//	pochodzenie.zmiennanull(organizmy[x -1+ (y)*max_x]);
						delete organizmy[x - 1 + (y)*max_x];
						organizmy[x - 1 + (y)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x - 1 + (y)*max_x]);
						this->x--;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x - 1 + (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
						//	pochodzenie.zmiennanull(organizmy[x - 1 + (y)*max_x]);
						delete organizmy[x - 1 + (y)*max_x];
						organizmy[x - 1 + (y)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);
					else if (zdarzenie == ROZMNAZAJ);
				}
			}
		}
		else if (ruch == PRAWO){
			if (this->x < max_x - 1){
				if ((organizmy[x + 1 + (y)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + 1 + (y)*max_x]);
					this->x++;
				}
				else {
					zdarzenie = organizmy[x + 1 + (y)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + 1 + (y)*max_x]);
						this->x++;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + 1 + (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
						//		pochodzenie.zmiennanull(organizmy[x + 1 + (y)*max_x]);
						delete organizmy[x + 1 + (y)*max_x];
						organizmy[x + 1 + (y)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + 1 + (y)*max_x]);
						this->x++;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + 1 + (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
						//	pochodzenie.zmiennanull(organizmy[x + 1 + (y)*max_x]);
						delete organizmy[x + 1 + (y)*max_x];
						organizmy[x + 1 + (y)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);

					else if (zdarzenie == ROZMNAZAJ);
				}
			}
		}
		else if (ruch == 'l'){
			castSkill();
		}
	}
	int Czlowiek::kolizja(Zwierze* kolizujacy){
		if (kolizujacy->getSila() >= this->sila){
			return ZABIJ;
		}
		else if (kolizujacy->getSila() < this->sila){
			return UMIERAJ;
		}
	}
	void Czlowiek::castSkill(){
		if (cooldown == 0){
			if (id == 1){
				risePower();
			}
		}
		else {
			std::ostringstream inttostring;
			std::string komunikat;
			komunikat += "#UMIEJETNOSCI NA COOLDOWNIE";
			pochodzenie.wypiszkomunikat(komunikat);
		}
	}
	void Czlowiek::risePower(){
		cooldown = 10;
		sila = sila + 5;
		powerbuff = 5;
		std::ostringstream inttostring;
		std::string komunikat;
		komunikat += "#SKILL MAGICZNY ELIKSIR On";
		pochodzenie.wypiszkomunikat(komunikat);

	}
}