/*#ifndef ORGANIZM_H
#define ORGANIZM_H

*/
#pragma once
namespace project{
	class Zwierze;
	class Swiat;
	class Organizm{
	protected:
		int ruch;
		int id;
		char* nazwa;
		int sila;
		int inicjatywa;
		int x;
		int y;
		int wiek;
		Swiat &pochodzenie;
	public:
		bool operator==(Organizm *other);
		int getSila();
		int getInicjatywa();
		int getWiek();
		int getId();
		int getX();
		int getY();
		void setSila(int x);
		void setWiek(int x);
		void setRuch(int ruch);
		virtual void akcja() = 0;
		virtual int kolizja(Zwierze *kolizujacy)=0;
		virtual char rysowanie() = 0;
		Organizm(Organizm &stary);
		Organizm(int xx, int yy, int idd, int silaa, int inicjatywaa, char *wyraz, int wiekk, Swiat &swiat);
		virtual ~Organizm() = 0;
	};
	
}
//#endif