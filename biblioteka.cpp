//using namespace std;
#include "biblioteka.h"
#include "Organizm.h"
#include "Roslina.h"
#include "Zwierze.h"
#include "trawa.h"
#include "Mlecz.h"
#include "wilczajagoda.h"
#include "Guarana.h"
#include "wilk.h"
#include "owca.h"
#include "lis.h"
#include "zolw.h"
#include "antylopa.h"
#include "czlowiek.h"
#include <fstream>
#include "wyjatek.h"
namespace project{
	int Swiat::getXsize(){
		return max_x;
	}
	int Swiat::getYsize(){
		return max_y;
	}
	int Swiat::losujIndex(){
		int losuj;
		do{
			losuj = rand() % (max_x*max_y);
		} while (organizmy[losuj] != NULL);
		return losuj;
	}
	void Swiat::transferujSwiat(Swiat &stary){
		this->max_x = stary.getXsize();
		this->max_y = stary.getYsize();
		this->organizmy = new Organizm*[max_x*max_y];
		this->kolejnosc = new Organizm*[max_x*max_y];
		for (int i = 0; i < this->max_x*this->max_y; i++){
			swappointers(&stary.organizmy[i], &this->organizmy[i]);
			stary.organizmy[i] = NULL;
			stary.kolejnosc[i] = NULL;
			this->kolejnosc[i] = NULL;
		}
		size_active = 0;
		Czas_Krok.czas = clock();
		Czas_Krok.kroki = 0;

	}
	Swiat::Swiat(Swiat &stary){
		this->max_x = stary.getXsize();
		this->max_y = stary.getYsize();
		this->organizmy = new Organizm*[max_x*max_y];
		this->kolejnosc = new Organizm*[max_x*max_y];
		for (int i = 0; i < this->max_x*this->max_y; i++){
			if (stary.organizmy[i]){
				if (stary.organizmy[i]->getId() == WILK) {
					this->organizmy[i] = new  Wilk(stary.organizmy[i]->getX(), stary.organizmy[i]->getY(), stary);
					this->organizmy[i]->setSila(stary.organizmy[i]->getSila());
				}
				else if (stary.organizmy[i]->getId() == OWCA) {
					this->organizmy[i] = new  Owca(stary.organizmy[i]->getX(), stary.organizmy[i]->getY(), stary);
					this->organizmy[i]->setSila(stary.organizmy[i]->getSila());
				}
				else if (stary.organizmy[i]->getId() == ZOLW){
					this->organizmy[i] = new  Zolw(stary.organizmy[i]->getX(), stary.organizmy[i]->getY(), stary);
					this->organizmy[i]->setSila(stary.organizmy[i]->getSila());
				}
				else if (stary.organizmy[i]->getId() == LIS){
					this->organizmy[i] = new Lis(stary.organizmy[i]->getX(), stary.organizmy[i]->getY(), stary);
					this->organizmy[i]->setSila(stary.organizmy[i]->getSila());
				}
				else if (stary.organizmy[i]->getId() == ANTYLOPA){
					this->organizmy[i] = new Antylopa(stary.organizmy[i]->getX(), stary.organizmy[i]->getY(), stary);
					this->organizmy[i]->setSila(stary.organizmy[i]->getSila());
				}
				else if (stary.organizmy[i]->getId() < 10){
					this->organizmy[i] = new Czlowiek(stary.organizmy[i]->getX(), stary.organizmy[i]->getY(), stary);
					this->organizmy[i]->setSila(stary.organizmy[i]->getSila());
				}
				else if (stary.organizmy[i]->getId() == WILCZAJAGODA){
					this->organizmy[i] = new WilczaJagoda(stary.organizmy[i]->getX(), stary.organizmy[i]->getY(), stary);
				}
				else if (stary.organizmy[i]->getId() == MLECZ){
					this->organizmy[i] = new Mlecz(stary.organizmy[i]->getX(), stary.organizmy[i]->getY(), stary);
				}
				else if (stary.organizmy[i]->getId() == GUARANA){
					this->organizmy[i] = new Guarana(stary.organizmy[i]->getX(), stary.organizmy[i]->getY(), stary);
				}
				else if (stary.organizmy[i]->getId() == TRAWA){
					this->organizmy[i] = new Trawa(stary.organizmy[i]->getX(), stary.organizmy[i]->getY(), stary);
				}

			}
			else this->organizmy[i] = NULL;
			stary.kolejnosc[i] = NULL;
			this->kolejnosc[i] = NULL;
		}
		size_active = 0;
		Czas_Krok.czas = clock();
		Czas_Krok.kroki = 0;
	}
	Swiat::~Swiat(){
		if (organizmy){
			for (int i = 0; i < max_x*max_y; i++){
				if (organizmy[i])
					delete organizmy[i];
			}
			delete[] kolejnosc;
			delete[] organizmy;
		}
		
	}
	Organizm** Swiat::getOrg(){
		return organizmy;
	}
	Organizm** Swiat::getKol(){
		return kolejnosc;
	}
	int Swiat::getSizeOfHeap(){
		return size_active;
	}
	Organizm* Swiat::seekHuman(){
		for (int i = 0; i < max_x*max_y; i++){
			if (organizmy[i]){
				if (organizmy[i]->getId() < 6){
					return organizmy[i];
				}
			}
		}
		return NULL;
	}
	Swiat::Swiat(){
		max_x =MAX_X;
		max_y = MAX_Y;
		kolejnosc = new Organizm*[max_y*max_x]{};
		organizmy = new Organizm*[max_y*max_x]{};
		size_active = 0;
		dodajOrganizmy();
	}

	void Swiat::ustawSwiat(){
		Czas_Krok.czas = clock();
		Czas_Krok.kroki = 0;
		clrscr();
		dodajKrok();
		dodajCzas();
		rysujMape();
		rysujSwiat();
		rysuj_menu();
	}
	void Swiat::rysujSwiat(){
		for (int i = 0; i < max_x*max_y; i++){
			if (organizmy[i] == NULL) {
				gotoxy((i%max_x)+STARTX+1, (i / max_x)+STARTY+1);
				wprintf(L" ");
			}
			else {
				gotoxy((i%max_x) + STARTX + 1, (i / max_x) + STARTY + 1);
				if (organizmy[i]->getId() >= 100 && organizmy[i]->getId() <200 ) textcolor(LIGHTGREEN);
				if (organizmy[i]->getId() >= 200 && organizmy[i]->getId() <300) textcolor(RED);
				if (organizmy[i]->getId() >= 0 && organizmy[i]->getId() <100) textcolor(YELLOW);
				wprintf(L"%c",organizmy[i]->rysowanie());
				textcolor(LIGHTGRAY);
			}
		}
		dodajCzas();
		dodajKrok();
	}
	void Swiat::dodajKrok(){
		gotoxy(MENUX - 10, MENUY + 3);
		Swiat::Czas_Krok.kroki++;
		textcolor(GREEN);
		wprintf(L"%d", Czas_Krok.kroki);
		textcolor(LIGHTGRAY);
	}
	void Swiat::dodajCzas(){ //pokazuje czas 
		gotoxy(MENUX - 10, MENUY + 1);
		textcolor(GREEN);
		wprintf(L"%.2f s", (double)(clock() - (Czas_Krok.czas)) / CLOCKS_PER_SEC);
		textcolor(LIGHTGRAY);
	}
	void Swiat::dodajOrganizmy(){
		int losuj = losujIndex();
		Swiat &me = *this;
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Mlecz(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Mlecz(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Mlecz(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Mlecz(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new WilczaJagoda(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new WilczaJagoda(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new WilczaJagoda(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Guarana(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Guarana(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Guarana(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Wilk(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Wilk(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Wilk(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Owca(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Owca(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Owca(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Lis(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Lis(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Lis(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Zolw(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Zolw(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Zolw(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Antylopa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Antylopa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Antylopa(losuj%max_x, losuj / max_x, me);
		losuj = losujIndex();
		organizmy[losuj] = new Czlowiek(losuj%max_x, losuj / max_x, me);
	}
	void Swiat::ustawStos(){
		for (int i = 0; i < max_x*max_y; i++){
			if (organizmy[i]!=NULL)
				heap_insert(organizmy[i]);
		}
	}
	void Swiat::wykonajTure(){
		Organizm *rm;
		Organizm *czlowiek;
		int ruch;
		ustawSwiat();
		do {
			czlowiek = seekHuman();
			if (czlowiek != NULL){
				do{
					ruch = getch();
					if (ruch == 0 || ruch == 224)
						ruch = 256 + getch();
				} while (ruch != LEWO && ruch != PRAWO && ruch != GORA && ruch != DOL && ruch != 'l' && ruch!='s' && ruch!='w' && ruch!='q');
				if (ruch == 's') {
					czyscpodmenu();
					int szukaj;
					std::fstream plik;
					std::string nazwa;
					zapisznazwe(nazwa);
					nazwa.erase(nazwa.length() - 1, 1);
					szukaj = nazwa.rfind(".txt");
					if (szukaj < 0) nazwa = nazwa + ".txt"; //sprawdzanie czy nazwa posiada .txt automatycznie dodaj gdy nie ma
					nazwa = nazwa + '\0';
					plik.open(nazwa, std::ios::out);
					wypisz(plik);
					plik.close();
					std::string komunikat;
					czyscpodmenu();
					komunikat += "Zapisales plik o nazwie ";
					komunikat += nazwa;
					wypiszkomunikat(komunikat);
				}
				else if (ruch == 'w'){

					std::fstream plik;
					std::string nazwa;
					plik.exceptions(std::ios::failbit);
					try{
						czyscpodmenu();
						int szukaj;
						zapisznazwe(nazwa);
						nazwa.erase(nazwa.length() - 1, 1);
						szukaj = nazwa.rfind(".txt");
						if (szukaj < 0) nazwa = nazwa + ".txt"; //sprawdzanie czy nazwa posiada .txt automatycznie dodaj gdy nie ma
						nazwa = nazwa + '\0';
						plik.open(nazwa, std::ios::in);
						try{
							zapisz(plik);
							ustawSwiat();
							std::string komunikat;
							czyscpodmenu();
							komunikat += "Wczytales plik o nazwie ";
							komunikat += nazwa;
							wypiszkomunikat(komunikat);
						}
						catch (Wyjatek &w){
							wypiszkomunikat(w.tekst);
						}
						plik.close();
					}
					catch (std::ios_base::failure &fail){
						std::string komunikat;
						czyscpodmenu();
						komunikat += "Nie udalo sie otworzyc pliku ";
						komunikat += nazwa;
						wypiszkomunikat(komunikat);
					}
				}
				else if (ruch == 'q'){
					exit(0);
				}
			}
			if (ruch != 's' && ruch != 'w' && ruch!='q'){
				czlowiek->setRuch(ruch);
				ustawStos();
				czyscpodmenu();
				while (size_active != 0){
					rm = heap_remove();
					kolejnosc[size_active] = NULL;
					if (rm)
						rm->akcja();
				}

				rysujSwiat();
			}
		} while (czlowiek = seekHuman());
		czyscpodmenu();
		std::string komunikat;
		komunikat += "ZGINALES!!! GRA ZAMKNIE SIE";
		wypiszkomunikat(komunikat);
		Sleep(4000);
	}
	void Swiat::rysuj_menu(){   //rysowanie menu z helpem
		int i = 1;
		gotoxy(MENUX - 20, MENUY);
		wprintf(L"CZAS");
		gotoxy(MENUX - 20, MENUY + 2);
		wprintf(L"KROKI");
		gotoxy(MENUX - 10, MENUY+4);
		wprintf(L"MAPA X Y");
		gotoxy(MENUX - 10, MENUY + 5);
		wprintf(L"%d %d",max_x,max_y);
		gotoxy(MENUX, MENUY);
		wprintf(L"KLAWISZE");
		gotoxy(MENUX, MENUY + i);
		wprintf(L"q=wyj�cie");
		i++;
		gotoxy(MENUX, MENUY + i);
		wprintf(L"strza�ki=poruszanie");
		i++;
		gotoxy(MENUX, MENUY + i);
		wprintf(L"l=umiejetnosc");
		i++;
		gotoxy(MENUX, MENUY + i);
		wprintf(L"s=zapisz(txt)");
		i++;
		gotoxy(MENUX, MENUY + i);
		wprintf(L"w=wczytaj(txt)");
	}
	std::string Swiat::zapisznazwe(std::string &nazwa){
		//zapisuje nazwe pliku wczytywanego lub zapisywanego
		char pomznak;
		gotoxy(PODMENUX, PODMENUY);
		wprintf(L"Podaj nazw�, pliku");
		gotoxy(PODMENUX, PODMENUY + 1);
		wprintf(L"(nazwa.rozsz)");
		gotoxy(PODMENUX, PODMENUY + 2);
		do{
			//wyswietlanie automatyczne tego co wpisujemy
			pomznak = getch();
			if (pomznak == 127 || pomznak == 8){
				if (nazwa.length() > 0) nazwa.erase(nazwa.length() - 1, 1);
				gotoxy(PODMENUX, PODMENUY + 2);
				wprintf(L"                                                               ");
				gotoxy(PODMENUX, PODMENUY + 2);
				for (unsigned int i = 0; i < nazwa.length(); i++) putch(nazwa[i]);
			}
			else if (nazwa.length()<20){
				putch(pomznak);
				nazwa = nazwa + pomznak;
			}
		} while (pomznak != 13);
		nazwa.erase(nazwa.length() - 1, 1);
		nazwa = nazwa + '\0';
		return nazwa;
	}
	void Swiat::czyscpodmenu(){
		//czysci miejsce w ktroym umieszczane sa komunikaty
		gotoxy(PODMENUX, PODMENUY);
		wprintf(L"                                                ");   //27 znakow
		gotoxy(PODMENUX, PODMENUY+1);
		wprintf(L"                                                ");
		gotoxy(PODMENUX, PODMENUY+2);
		wprintf(L"                                                ");
		gotoxy(PODMENUX, PODMENUY - 3);
		wprintf(L"                                                ");
	}
	void Swiat::czysckomunikat(){
		int i = 0;
		//czysci miejsce w ktroym umieszczane sa komunikaty
		gotoxy(4, MAX_Y + 7);
		wprintf(L"                                                ");   //27 znakow
		i++;
		gotoxy(4, MAX_Y + 7+i);
		wprintf(L"                                                ");
		i++;
		gotoxy(4, MAX_Y + 7+i);
		wprintf(L"                                                ");
		i++;
		gotoxy(4, MAX_Y + 7+i);
		wprintf(L"                                                ");
		i++;
		gotoxy(4, MAX_Y + 7 + i);
		wprintf(L"                                                ");
		i++;
		gotoxy(4, MAX_Y + 7 + i);
		wprintf(L"                                                ");
	}
	void Swiat::wypiszkomunikat(std::string &komunikat){
		if (komunikat[0] == '#'){
			gotoxy(PODMENUX, PODMENUY-3);
			for (int i = 0; i < komunikat.length(); i++){
				wprintf(L"%c", komunikat[i]);
			}
		}
		else{

			gotoxy(4,MAX_Y+ komnastepny+7);
			komnastepny++;
			for (int i = 0; i < komunikat.length(); i++){
				wprintf(L"%c", komunikat[i]);
			}
			if (komnastepny > 5){
				komnastepny = 0;
				czysckomunikat();
			}
			gotoxy(0, 0);
		}
	}
	void Swiat::rysujMape(){
		// rysowanie pustego sudoku
		//funkcja rysuje plansze gry za pomoc� wybranych znak�w z kodowania UTF-8
		// gdzie x0104 to x-heks(16) a reszta to znak zapisany szestnastkowo
		int posx = STARTX;
		int posy = STARTY;
		int x;
		int y;
		//pierwsze 3 bloki
		gotoxy(posx + 2, posy - 1);
		wprintf(L"Wirtualny Swiat");
		gotoxy(posx, posy);
		gotoxy(posx, posy);
		wprintf(L"\x2554");
		for (x = posx + 1; x < max_x + posx + 1; x++){
			gotoxy(x, posy);
			wprintf(L"\x2550");
		}
		gotoxy(x, posy);
		wprintf(L"\x2557");
		for (y = posy + 1; y < max_y + posy + 1; y++){
			gotoxy(posx, y);
			wprintf(L"\x2551");
			for (x = posx + 1; x < max_x + posx + 1; x++){
				gotoxy(x, y);
				wprintf(L"\x0020");
			}
			gotoxy(x, y);
			wprintf(L"\x2551");
		}
		gotoxy(posx, y);
		wprintf(L"\x255A");
		for (x = posx + 1; x < max_x + posx + 1; x++){
			gotoxy(x, y);
			wprintf(L"\x2550");
		}
		gotoxy(x, y);
		wprintf(L"\x255D");
	}
	void Swiat::wypisz(std::ostream & out){
		out << "#WIRTUALNY_SWIAT" << std::endl;
		out << "#ROZMIAR_X_Y" << std::endl;
		out << max_x << " " << max_y << std::endl;
		out << "#ORGANIZMY" << std::endl;
		for (int i = 0; i < max_x*max_y; i++){
			if (organizmy[i]) out << organizmy[i]->getId() <<" "<< organizmy[i]->getSila() <<" "<< organizmy[i]->getWiek()<<std::endl;
			else out << 9999 << std::endl;
		}
		out << "#KONIEC" << std::endl;
	}
	void Swiat::zapisz(std::istream & in){
		std::string znak,sila,wiek;
		in >> znak;
		if (znak == "#WIRTUALNY_SWIAT"){
			in >> znak;
			if (znak == "#ROZMIAR_X_Y"){
				in >> znak;
				if (atoi(znak.c_str()) <= 40){
					max_x = atoi(znak.c_str());
				}
				else throw Wyjatek("za duza plansza(max_x=40,max_y=18)");
				in >> znak;
				if (atoi(znak.c_str()) <= 18){
					max_y = atoi(znak.c_str());
				}
				else throw Wyjatek("za duza plansza(max_x=40,max_y=18)");
			}
			else throw Wyjatek("Brak #ROZMIAR_X_Y W PLIKU");
			in >> znak;
			if (znak == "#ORGANIZMY"){
				std::string posx, posy;
				Swiat &me = *this;
				Organizm **organizmy = new Organizm*[max_x*max_y];
				Organizm **kolejnosc = new Organizm*[max_x*max_y];
				for (int i = 0; i < max_x*max_y;){
					in >> znak;
					if (!znak.empty()){
						if (znak == "9999" || znak=="#KONIEC"){
							organizmy[i] = NULL;
							i++;
						}
						else {
							if (atoi(znak.c_str()) < 10) {
								organizmy[i] = new Czlowiek(i%max_x, i / max_x, me);
								in >> znak;
								organizmy[i]->setSila(atoi(znak.c_str()));
								in >> znak;
								organizmy[i]->setWiek(atoi(znak.c_str()));
							}
							else if (atoi(znak.c_str()) == TRAWA) {
								organizmy[i] = new Trawa(i%max_x, i / max_x, me);
								in >> znak;
								organizmy[i]->setSila(atoi(znak.c_str()));
								in >> znak;
								organizmy[i]->setWiek(atoi(znak.c_str()));
							}
							else if (atoi(znak.c_str()) == MLECZ) {
								organizmy[i] = new Mlecz(i%max_x, i / max_x, me);
								in >> znak;
								organizmy[i]->setSila(atoi(znak.c_str()));
								in >> znak;
								organizmy[i]->setWiek(atoi(znak.c_str()));
							}
							else if (atoi(znak.c_str()) == GUARANA){
								organizmy[i] = new Guarana(i%max_x, i / max_x, me);
								in >> znak;
								organizmy[i]->setSila(atoi(znak.c_str()));
								in >> znak;
								organizmy[i]->setWiek(atoi(znak.c_str()));
							}
							else if (atoi(znak.c_str()) == WILCZAJAGODA){
								organizmy[i] = new WilczaJagoda(i%max_x, i / max_x, me);
								in >> znak;
								organizmy[i]->setSila(atoi(znak.c_str()));
								in >> znak;
								organizmy[i]->setWiek(atoi(znak.c_str()));
							}
							else if (atoi(znak.c_str()) == WILK) {
								organizmy[i] = new Wilk(i%max_x, i / max_x, me);
								in >> znak;
								organizmy[i]->setSila(atoi(znak.c_str()));
								in >> znak;
								organizmy[i]->setWiek(atoi(znak.c_str()));
							}
							else if (atoi(znak.c_str()) == OWCA) {
								organizmy[i] = new Owca(i%max_x, i / max_x, me);
								in >> znak;
								organizmy[i]->setSila(atoi(znak.c_str()));
								in >> znak;
								organizmy[i]->setWiek(atoi(znak.c_str()));
							}
							else if (atoi(znak.c_str()) == LIS) {
								organizmy[i] = new Lis(i%max_x, i / max_x, me);
								in >> znak;
								organizmy[i]->setSila(atoi(znak.c_str()));
								in >> znak;
								organizmy[i]->setWiek(atoi(znak.c_str()));
							}
							else if (atoi(znak.c_str()) == ZOLW) {
								organizmy[i] = new Zolw(i%max_x, i / max_x, me);
								in >> znak;
								organizmy[i]->setSila(atoi(znak.c_str()));
								in >> znak;
								organizmy[i]->setWiek(atoi(znak.c_str()));
							}
							else if (atoi(znak.c_str()) == ANTYLOPA){
								organizmy[i] = new Antylopa(i%max_x, i / max_x, me);
								in >> znak;
								organizmy[i]->setSila(atoi(znak.c_str()));
								in >> znak;
								organizmy[i]->setWiek(atoi(znak.c_str()));
							}
							else throw Wyjatek("Blad odczytu Organizmow");
							i++;
						}
					}
					else {
						organizmy[i] = NULL;
						i++;
					}
				}
				Swiat::~Swiat();
				size_active = 0;
				this->organizmy = organizmy;
				this->kolejnosc = kolejnosc;

			}
			else throw Wyjatek("Brak #ORGANIZMY");
		}
		else throw Wyjatek("Brak #WIRTUALNY_SWIAT");
	}
	std::ostream& operator<<(std::ostream & left, Swiat& right){
		right.wypisz(left); 
		return left;
	}
	std::istream& operator>>(std::istream & left, Swiat& right){
		right.zapisz(left);
		return left;
	}



	void Swiat::swappointers(Organizm** a, Organizm** b){
		Organizm *temp = *a;
		*a = *b;
		*b = temp;

	}
	void Swiat::heapify(int &i, int &size){
		int maxps;
		int L = 2 * i;
		int P = 2 * i + 1;
		if (kolejnosc[i - 1] == NULL) {
			if (L <= size && kolejnosc[L - 1] != NULL) maxps = L;
			else if (P <= size && kolejnosc[P - 1] != NULL) maxps = P;
			else maxps = i;
		}
		else {
			if (L <= size && kolejnosc[L - 1] == NULL) maxps = L;
			else if (L <= size && kolejnosc[L - 1]->getInicjatywa() > kolejnosc[i - 1]->getInicjatywa()) maxps = L;
			else maxps = i;
			if (L <= size && kolejnosc[L - 1] == NULL) maxps = L;
			else if (L <= size && kolejnosc[L - 1]->getInicjatywa() == kolejnosc[i - 1]->getInicjatywa()){
				if (L <= size && kolejnosc[L - 1]->getWiek() > kolejnosc[i - 1]->getWiek()) {
					maxps = L;
				}
			}
			if (P <= size && kolejnosc[P - 1] == NULL) maxps = P;
			else if (P <= size && kolejnosc[P - 1]->getInicjatywa() > kolejnosc[i - 1]->getInicjatywa()) maxps = P;
			else if (P <= size && kolejnosc[P - 1]->getInicjatywa() == kolejnosc[i - 1]->getInicjatywa()){
				if (P <= size && kolejnosc[P - 1]->getWiek() > kolejnosc[i - 1]->getWiek())  maxps = P;
			}
		}
		if (maxps != i){
			Swiat::swappointers(&kolejnosc[i - 1], &kolejnosc[maxps - 1]);
			heapify(maxps, size);
		}
	}
	void Swiat::heap_build(){
		for (int i = size_active / 2; i > 0; i /= 2){
			heapify(i,this->size_active);
		}
	}
	void Swiat::heap_insert(Organizm *x){
		kolejnosc[size_active] = x; //wstaw na koniec
		size_active++;
		heap_build();
	}
	Organizm* Swiat::heap_remove(){
		if (size_active == 0)
			return NULL;
		else if (kolejnosc[0]==NULL) {
			--size_active;
			Swiat::swappointers(&kolejnosc[0], &kolejnosc[size_active]);
			heap_build();
			return NULL;
		}
		--size_active;
		Swiat::swappointers(&kolejnosc[0], &kolejnosc[size_active]);
		heap_build();
		return kolejnosc[size_active];
	}

	/*void Swiat::zmiennanull(Organizm *x){
	for (int i = 0; i < size_active; i++){
	if (x == kolejnosc[i]){
	kolejnosc[i] = NULL;
	break;
	}
	}
	}*/
}