#pragma once
#include "Zwierze.h"
#include "biblioteka.h"
#include "wilk.h"
#include "owca.h"
#include "zolw.h"
namespace project{
	void Zwierze::akcja(){
		int max_x = pochodzenie.getXsize();
		int max_y = pochodzenie.getYsize();
		Organizm **organizmy = pochodzenie.getOrg();
		Organizm **kolejnosc = pochodzenie.getKol();
		int size_active = pochodzenie.getSizeOfHeap();
		int ruch;
		int zdarzenie;
		ruch = rand() % 4;
		switch (ruch){
		case 0:{
			if (this->y > 0){
				if ((organizmy[x + (y - 1)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y - 1)*max_x]);
					this->y--;
				}
				else {
					zdarzenie = organizmy[x + (y - 1)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y - 1)*max_x]);
						this->y--;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y - 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x + (y - 1)*max_x]);
						delete organizmy[x + (y - 1)*max_x];
						organizmy[x + (y - 1)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y - 1)*max_x]);
						this->y--;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y - 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x + (y - 1)*max_x]);
						delete organizmy[x + (y - 1)*max_x];
						organizmy[x + (y - 1)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);
					else if (zdarzenie == ROZMNAZAJ){
						char *wynik = new char[9]{};
						int proba = 0;
						if (this->x > 0 && this->y > 0){
							if (organizmy[x - 1 + (y - 1)*max_x] == NULL){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (this->y > 0){
							if ((organizmy[x + (y - 1)*max_x] == NULL)){
								wynik[proba] = '2';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y > 0){
							if ((organizmy[x + 1 + (y - 1)*max_x] == NULL)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (this->x > 0){
							if ((organizmy[x - 1 + (y)*max_x] == NULL)){
								wynik[proba] = '4';
								proba++;
							}
						}
						if (this->x < max_x - 1){
							if ((organizmy[x + 1 + (y)*max_x] == NULL)){
								wynik[proba] = '5';
								proba++;
							}
						}
						if (this->x > 0 && this->y < max_y - 1){
							if ((organizmy[x - 1 + (y + 1)*max_x] == NULL)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (this->y < max_y - 1){
							if ((organizmy[x + (y + 1)*max_x] == NULL)){
								wynik[proba] = '7';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y < max_y - 1){
							if ((organizmy[x +1+ (y + 1)*max_x] == NULL)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = NULL;
						if (wynik[0] != NULL){
							for (proba = 1; wynik[proba] != NULL; proba++);
							proba = rand() % (proba);
							switch (wynik[proba]){
							case '1':{
									if (id == WILK) organizmy[x - 1 + (y - 1)*max_x] = new  Wilk(x - 1, y - 1, pochodzenie);
									else if (id == OWCA) organizmy[x - 1 + (y - 1)*max_x] = new  Owca(x - 1, y - 1, pochodzenie);
									else if (id == ZOLW) organizmy[x - 1 + (y - 1)*max_x] = new  Zolw(x - 1, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y - 1)*max_x] = new  WilczaJagoda(x - 1, y - 1, pochodzenie);
							}
								break;
							case '2':{
									if (id == WILK) organizmy[x + (y - 1)*max_x] = new  Wilk(x, y - 1, pochodzenie);
									else if (id == OWCA) organizmy[x + (y - 1)*max_x] = new  Owca(x, y - 1, pochodzenie);
									else if (id == ZOLW) organizmy[x + (y - 1)*max_x] = new  Zolw(x, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x + (y - 1)*max_x] = new  WilczaJagoda(x, y - 1, pochodzenie);

							}
								break;
							case '3':{
									if (id == WILK) organizmy[x + 1 + (y - 1)*max_x] = new  Wilk(x + 1, y - 1, pochodzenie);
									else if (id == OWCA) organizmy[x + 1 + (y - 1)*max_x] = new  Owca(x + 1, y - 1, pochodzenie);
									else if (id == ZOLW) organizmy[x + 1 + (y - 1)*max_x] = new  Zolw(x + 1, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x + 1 + (y - 1)*max_x] = new  WilczaJagoda(x + 1, y - 1, pochodzenie);
							}
								break;
							case '4':{
									if (id == WILK) organizmy[x - 1 + (y)*max_x] = new  Wilk(x - 1, y, pochodzenie);
									else if (id == OWCA) organizmy[x - 1 + (y)*max_x] = new  Owca(x - 1, y, pochodzenie);
									else if (id == ZOLW) organizmy[x - 1 + (y)*max_x] = new  Zolw(x - 1, y, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y)*max_x] = new  WilczaJagoda(x - 1, y, pochodzenie);
							}
								break;
							case '5':{
									if (id == WILK) organizmy[x + 1 + (y)*max_x] = new  Wilk(x + 1, y, pochodzenie);
									else if (id == OWCA) organizmy[x + 1 + (y)*max_x] = new  Owca(x + 1, y, pochodzenie);
									else if (id == ZOLW) organizmy[x + 1 + (y)*max_x] = new  Zolw(x + 1, y, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y)*max_x] = new  WilczaJagoda(x + 1, y, pochodzenie);
							}
								break;
							case '6':{
									if (id == WILK) organizmy[x - 1 + (y + 1)*max_x] = new  Wilk(x - 1, y + 1, pochodzenie);
									else if (id == OWCA) organizmy[x - 1 + (y + 1)*max_x] = new  Owca(x - 1, y + 1, pochodzenie);
									else if (id == ZOLW) organizmy[x - 1 + (y + 1)*max_x] = new  Zolw(x - 1, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y + 1)*max_x] = new  WilczaJagoda(x - 1, y + 1, pochodzenie);
							}
								break;
							case '7':{
									if (id == WILK) organizmy[x + (y + 1)*max_x] = new  Wilk(x, y + 1, pochodzenie);
									else if (id == OWCA) organizmy[x + (y + 1)*max_x] = new  Owca(x, y + 1, pochodzenie);
									else if (id == ZOLW) organizmy[x + (y + 1)*max_x] = new  Zolw(x, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x + (y + 1)*max_x] = new  WilczaJagoda(x, y + 1, pochodzenie);
							}
								break;
							case '8':{
									if (id == WILK) organizmy[x + 1 + (y + 1)*max_x] = new  Wilk(x + 1, y + 1, pochodzenie);
									else if (id == OWCA) organizmy[x + 1 + (y + 1)*max_x] = new  Owca(x + 1, y + 1, pochodzenie);
									else if (id == ZOLW) organizmy[x + 1 + (y + 1)*max_x] = new  Zolw(x + 1, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x + 1 + (y + 1)*max_x] = new  WilczaJagoda(x + 1, y + 1, pochodzenie);
							}
								break;
							}
							zmeczony = 10;
						}
						if (wynik) delete wynik;
					}
				}
			}
		}
		break;
		case 1:{
			 if (this->y <max_y-1){
				if ((organizmy[x + (y + 1)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y + 1)*max_x]);
					this->y++;
				}
				else {
					zdarzenie = organizmy[x + (y + 1)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y + 1)*max_x]);
						this->y++;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y +1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x + (y + 1)*max_x]);
						delete organizmy[x + (y + 1)*max_x];
						organizmy[x + (y + 1)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y + 1)*max_x]);
						this->y++;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y + 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
				//		pochodzenie.zmiennanull(organizmy[x + (y + 1)*max_x]);
						delete organizmy[x + (y + 1)*max_x];
						organizmy[x + (y + 1)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);
					else if (zdarzenie == ROZMNAZAJ){
						char *wynik = new char[9]{};
						int proba = 0;
						if (this->x > 0 && this->y > 0){
							if (organizmy[x - 1 + (y - 1)*max_x] == NULL){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (this->y > 0){
							if ((organizmy[x + (y - 1)*max_x] == NULL)){
								wynik[proba] = '2';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y > 0){
							if ((organizmy[x + 1 + (y - 1)*max_x] == NULL)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (this->x > 0){
							if ((organizmy[x - 1 + (y)*max_x] == NULL)){
								wynik[proba] = '4';
								proba++;
							}
						}
						if (this->x < max_x - 1){
							if ((organizmy[x + 1 + (y)*max_x] == NULL)){
								wynik[proba] = '5';
								proba++;
							}
						}
						if (this->y< max_y - 1){
							if ((organizmy[x + (y + 1)*max_x] == NULL)){
								wynik[proba] = '7';
								proba++;
							}
						}
						if (this->x > 0 && this->y < max_y - 1){
							if ((organizmy[x - 1 + (y + 1)*max_x] == NULL)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y < max_y - 1){
							if ((organizmy[x + 1+(y + 1)*max_x] == NULL)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = NULL;
						if (wynik[0] != NULL){
							for (proba = 1; wynik[proba] != NULL; proba++);
							proba = rand() % (proba);
							switch (wynik[proba]){
							case '1':{
								if (id == WILK) organizmy[x - 1 + (y - 1)*max_x] = new  Wilk(x - 1, y - 1, pochodzenie);
								else if (id == OWCA) organizmy[x - 1 + (y - 1)*max_x] = new  Owca(x - 1, y - 1, pochodzenie);
								else if (id == ZOLW) organizmy[x - 1 + (y - 1)*max_x] = new  Zolw(x - 1, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y - 1)*max_x] = new  WilczaJagoda(x - 1, y - 1, pochodzenie);
							}
								break;
							case '2':{
								if (id == WILK) organizmy[x + (y - 1)*max_x] = new  Wilk(x, y - 1, pochodzenie);
								else if (id == OWCA) organizmy[x + (y - 1)*max_x] = new  Owca(x, y - 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + (y - 1)*max_x] = new  Zolw(x, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x + (y - 1)*max_x] = new  WilczaJagoda(x, y - 1, pochodzenie);

							}
								break;
							case '3':{
								if (id == WILK) organizmy[x + 1 + (y - 1)*max_x] = new  Wilk(x + 1, y - 1, pochodzenie);
								else if (id == OWCA) organizmy[x + 1 + (y - 1)*max_x] = new  Owca(x + 1, y - 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + 1 + (y - 1)*max_x] = new  Zolw(x + 1, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x + 1 + (y - 1)*max_x] = new  WilczaJagoda(x + 1, y - 1, pochodzenie);
							}
								break;
							case '4':{
								if (id == WILK) organizmy[x - 1 + (y)*max_x] = new  Wilk(x - 1, y, pochodzenie);
								else if (id == OWCA) organizmy[x - 1 + (y)*max_x] = new  Owca(x - 1, y, pochodzenie);
								else if (id == ZOLW) organizmy[x - 1 + (y)*max_x] = new  Zolw(x - 1, y, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y)*max_x] = new  WilczaJagoda(x - 1, y, pochodzenie);
							}
								break;
							case '5':{
								if (id == WILK) organizmy[x + 1 + (y)*max_x] = new  Wilk(x + 1, y, pochodzenie);
								else if (id == OWCA) organizmy[x + 1 + (y)*max_x] = new  Owca(x + 1, y, pochodzenie);
								else if (id == ZOLW) organizmy[x + 1 + (y)*max_x] = new  Zolw(x + 1, y, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y)*max_x] = new  WilczaJagoda(x + 1, y, pochodzenie);
							}
								break;
							case '6':{
								if (id == WILK) organizmy[x - 1 + (y + 1)*max_x] = new  Wilk(x - 1, y + 1, pochodzenie);
								else if (id == OWCA) organizmy[x - 1 + (y + 1)*max_x] = new  Owca(x - 1, y + 1, pochodzenie);
								else if (id == ZOLW) organizmy[x - 1 + (y + 1)*max_x] = new  Zolw(x - 1, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y + 1)*max_x] = new  WilczaJagoda(x - 1, y + 1, pochodzenie);
							}
								break;
							case '7':{
								if (id == WILK) organizmy[x + (y + 1)*max_x] = new  Wilk(x, y + 1, pochodzenie);
								else if (id == OWCA) organizmy[x + (y + 1)*max_x] = new  Owca(x, y + 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + (y + 1)*max_x] = new  Zolw(x, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x + (y + 1)*max_x] = new  WilczaJagoda(x, y + 1, pochodzenie);
							}
								break;
							case '8':{
								if (id == WILK) organizmy[x + 1 + (y + 1)*max_x] = new  Wilk(x + 1, y + 1, pochodzenie);
								else if (id == OWCA) organizmy[x + 1 + (y + 1)*max_x] = new  Owca(x + 1, y + 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + 1 + (y + 1)*max_x] = new  Zolw(x + 1, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x + 1 + (y + 1)*max_x] = new  WilczaJagoda(x + 1, y + 1, pochodzenie);
							}
								break;
							}
							zmeczony = 10;
						}
						if (wynik) delete wynik;
					}
				}

			}
		}
		break;
		case 2:{
			if (this->x >0){
				if ((organizmy[x -1+ (y )*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x -1 + (y)*max_x]);
					this->x--;
				}
				else {
					zdarzenie = organizmy[x -1+ (y)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x - 1 + (y)*max_x]);
						this->x--;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x -1+ (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x -1+ (y)*max_x]);
						delete organizmy[x -1+ (y )*max_x];
						organizmy[x -1+ (y)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x-1 + (y)*max_x]);
						this->x--;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x-1+ (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x - 1 + (y)*max_x]);
						delete organizmy[x -1+ (y )*max_x];
						organizmy[x -1+ (y)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);
					else if (zdarzenie == ROZMNAZAJ){
						char *wynik = new char[9]{};
						int proba = 0;
						if (this->x > 0 && this->y > 0){
							if (organizmy[x - 1 + (y - 1)*max_x] == NULL){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (this->y > 0){
							if ((organizmy[x + (y - 1)*max_x] == NULL)){
								wynik[proba] = '2';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y > 0){
							if ((organizmy[x + 1 + (y - 1)*max_x] == NULL)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (this->x > 0){
							if ((organizmy[x - 1 + (y)*max_x] == NULL)){
								wynik[proba] = '4';
								proba++;
							}
						}
						if (this->x < max_x - 1){
							if ((organizmy[x + 1 + (y)*max_x] == NULL)){
								wynik[proba] = '5';
								proba++;
							}
						}
						if (this->x > 0 && this->y < max_y - 1){
							if ((organizmy[x - 1 + (y + 1)*max_x] == NULL)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (this->y< max_y - 1){
							if ((organizmy[x + (y + 1)*max_x] == NULL)){
								wynik[proba] = '7';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y < max_y - 1){
							if ((organizmy[x +1+ (y + 1)*max_x] == NULL)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = NULL;
						if (wynik[0] != NULL){
							for (proba = 1; wynik[proba] != NULL; proba++);
							proba = rand() % (proba);
							switch (wynik[proba]){
							case '1':{
								if (id == WILK) organizmy[x - 1 + (y - 1)*max_x] = new  Wilk(x - 1, y - 1, pochodzenie);
								else if (id == OWCA) organizmy[x - 1 + (y - 1)*max_x] = new  Owca(x - 1, y - 1, pochodzenie);
								else if (id == ZOLW) organizmy[x - 1 + (y - 1)*max_x] = new  Zolw(x - 1, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y - 1)*max_x] = new  WilczaJagoda(x - 1, y - 1, pochodzenie);
							}
								break;
							case '2':{
								if (id ==WILK) organizmy[x + (y - 1)*max_x] = new  Wilk(x, y - 1, pochodzenie);
								else if (id ==OWCA) organizmy[x + (y - 1)*max_x] = new  Owca(x, y - 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + (y - 1)*max_x] = new  Zolw(x, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x + (y - 1)*max_x] = new  WilczaJagoda(x, y - 1, pochodzenie);

							}
								break;
							case '3':{
								if (id == WILK) organizmy[x + 1 + (y - 1)*max_x] = new  Wilk(x + 1, y - 1, pochodzenie);
								else if (id == OWCA) organizmy[x + 1 + (y - 1)*max_x] = new  Owca(x + 1, y - 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + 1 + (y - 1)*max_x] = new  Zolw(x + 1, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x + 1 + (y - 1)*max_x] = new  WilczaJagoda(x + 1, y - 1, pochodzenie);
							}
								break;
							case '4':{
								if (id == WILK) organizmy[x - 1 + (y)*max_x] = new  Wilk(x - 1, y, pochodzenie);
								else if (id == OWCA) organizmy[x - 1 + (y)*max_x] = new  Owca(x - 1, y, pochodzenie);
								else if (id == ZOLW) organizmy[x - 1 + (y)*max_x] = new  Zolw(x - 1, y, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y)*max_x] = new  WilczaJagoda(x - 1, y, pochodzenie);
							}
								break;
							case '5':{
								if (id == WILK) organizmy[x + 1 + (y)*max_x] = new  Wilk(x + 1, y, pochodzenie);
								else if (id == OWCA) organizmy[x + 1 + (y)*max_x] = new  Owca(x + 1, y, pochodzenie);
								else if (id == ZOLW) organizmy[x + 1 + (y)*max_x] = new  Zolw(x + 1, y, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y)*max_x] = new  WilczaJagoda(x + 1, y, pochodzenie);
							}
								break;
							case '6':{
								if (id == WILK) organizmy[x - 1 + (y + 1)*max_x] = new  Wilk(x - 1, y + 1, pochodzenie);
								else if (id == OWCA) organizmy[x - 1 + (y + 1)*max_x] = new  Owca(x - 1, y + 1, pochodzenie);
								else if (id == ZOLW) organizmy[x - 1 + (y + 1)*max_x] = new  Zolw(x - 1, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y + 1)*max_x] = new  WilczaJagoda(x - 1, y + 1, pochodzenie);
							}
								break;
							case '7':{
								if (id == WILK) organizmy[x + (y + 1)*max_x] = new  Wilk(x, y + 1, pochodzenie);
								else if (id == OWCA) organizmy[x + (y + 1)*max_x] = new  Owca(x, y + 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + (y + 1)*max_x] = new  Zolw(x, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x + (y + 1)*max_x] = new  WilczaJagoda(x, y + 1, pochodzenie);
							}
								break;
							case '8':{
								if (id == WILK) organizmy[x + 1 + (y + 1)*max_x] = new  Wilk(x + 1, y + 1, pochodzenie);
								else if (id == OWCA) organizmy[x + 1 + (y + 1)*max_x] = new  Owca(x + 1, y + 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + 1 + (y + 1)*max_x] = new  Zolw(x + 1, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x + 1 + (y + 1)*max_x] = new  WilczaJagoda(x + 1, y + 1, pochodzenie);
							}
								break;
							}
							zmeczony = 10;
						}
						if (wynik) delete wynik;
					}
				}

			}
		}
		break;
		case 3:{
			if (this->x <max_x-1){
				if ((organizmy[x +1 + (y)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + 1 + (y)*max_x]);
					this->x++;
				}
				else {
					zdarzenie = organizmy[x + 1 + (y)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + 1 + (y)*max_x]);
						this->x++;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x+1 + (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
				//		pochodzenie.zmiennanull(organizmy[x + 1 + (y)*max_x]);
 						delete organizmy[x + 1 + (y)*max_x];
						organizmy[x + 1 + (y)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x +1+ (y)*max_x]);
						this->x++;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x +1+ (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x + 1 + (y)*max_x]);
						delete organizmy[x + 1 + (y)*max_x];
						organizmy[x + 1 + (y)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);

					else if (zdarzenie == ROZMNAZAJ){
						char *wynik = new char[9]{};
						int proba = 0;
						if (this->x > 0 && this->y > 0){
							if (organizmy[x - 1 + (y - 1)*max_x] == NULL){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (this->y > 0){
							if ((organizmy[x + (y - 1)*max_x] == NULL)){
								wynik[proba] = '2';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y > 0){
							if ((organizmy[x + 1 + (y - 1)*max_x] == NULL)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (this->x > 0){
						if ((organizmy[x - 1 + (y)*max_x] == NULL)){
							wynik[proba] = '4';
							proba++;
							}
						}
						if (this->x < max_x - 1){
							if ((organizmy[x + 1 + (y)*max_x] == NULL)){
								wynik[proba] = '5';
								proba++;
							}
						}
						if (this->x > 0 && this->y < max_y - 1){
							if ((organizmy[x - 1 + (y + 1)*max_x] == NULL)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (this->y< max_y - 1){
							if ((organizmy[x + (y + 1)*max_x] == NULL)){
								wynik[proba] = '7';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y < max_y - 1){
							if ((organizmy[x + 1+(y + 1)*max_x] == NULL)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = NULL;
						if (wynik[0] != NULL){
							for (proba = 1; wynik[proba] != NULL; proba++);
							proba = rand() % (proba);
							switch (wynik[proba]){
							case '1':{
								if (id == WILK) organizmy[x - 1 + (y - 1)*max_x] = new  Wilk(x - 1, y - 1, pochodzenie);
								else if (id == OWCA) organizmy[x - 1 + (y - 1)*max_x] = new  Owca(x - 1, y - 1, pochodzenie);
								else if (id == ZOLW) organizmy[x - 1 + (y - 1)*max_x] = new  Zolw(x - 1, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y - 1)*max_x] = new  WilczaJagoda(x - 1, y - 1, pochodzenie);
							}
								break;
							case '2':{
								if (id == WILK) organizmy[x + (y - 1)*max_x] = new  Wilk(x, y - 1, pochodzenie);
								else if (id == OWCA) organizmy[x + (y - 1)*max_x] = new  Owca(x, y - 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + (y - 1)*max_x] = new  Zolw(x, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x + (y - 1)*max_x] = new  WilczaJagoda(x, y - 1, pochodzenie);

							}
								break;
							case '3':{
								if (id == WILK) organizmy[x + 1 + (y - 1)*max_x] = new  Wilk(x + 1, y - 1, pochodzenie);
								else if (id == OWCA) organizmy[x + 1 + (y - 1)*max_x] = new  Owca(x + 1, y - 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + 1 + (y - 1)*max_x] = new  Zolw(x + 1, y - 1, pochodzenie);
								//	else if (id == 103) organizmy[x + 1 + (y - 1)*max_x] = new  WilczaJagoda(x + 1, y - 1, pochodzenie);
							}
								break;
							case '4':{
								if (id == WILK) organizmy[x - 1 + (y)*max_x] = new  Wilk(x - 1, y, pochodzenie);
								else if (id == OWCA) organizmy[x - 1 + (y)*max_x] = new  Owca(x - 1, y, pochodzenie);
								else if (id == ZOLW) organizmy[x - 1 + (y)*max_x] = new  Zolw(x - 1, y, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y)*max_x] = new  WilczaJagoda(x - 1, y, pochodzenie);
							}
								break;
							case '5':{
								if (id == WILK) organizmy[x + 1 + (y)*max_x] = new  Wilk(x + 1, y, pochodzenie);
								else if (id == OWCA) organizmy[x + 1 + (y)*max_x] = new  Owca(x + 1, y, pochodzenie);
								else if (id == ZOLW) organizmy[x + 1 + (y)*max_x] = new  Zolw(x + 1, y, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y)*max_x] = new  WilczaJagoda(x + 1, y, pochodzenie);
							}
								break;
							case '6':{
								if (id == WILK) organizmy[x - 1 + (y + 1)*max_x] = new  Wilk(x - 1, y + 1, pochodzenie);
								else if (id == OWCA) organizmy[x - 1 + (y + 1)*max_x] = new  Owca(x - 1, y + 1, pochodzenie);
								else if (id == ZOLW) organizmy[x - 1 + (y + 1)*max_x] = new  Zolw(x - 1, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x - 1 + (y + 1)*max_x] = new  WilczaJagoda(x - 1, y + 1, pochodzenie);
							}
								break;
							case '7':{
								if (id == WILK) organizmy[x + (y + 1)*max_x] = new  Wilk(x, y + 1, pochodzenie);
								else if (id == OWCA) organizmy[x + (y + 1)*max_x] = new  Owca(x, y + 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + (y + 1)*max_x] = new  Zolw(x, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x + (y + 1)*max_x] = new  WilczaJagoda(x, y + 1, pochodzenie);
							}
								break;
							case '8':{
								if (id == WILK) organizmy[x + 1 + (y + 1)*max_x] = new  Wilk(x + 1, y + 1, pochodzenie);
								else if (id == OWCA) organizmy[x + 1 + (y + 1)*max_x] = new  Owca(x + 1, y + 1, pochodzenie);
								else if (id == ZOLW) organizmy[x + 1 + (y + 1)*max_x] = new  Zolw(x + 1, y + 1, pochodzenie);
								//	else if (id == 103) organizmy[x + 1 + (y + 1)*max_x] = new  WilczaJagoda(x + 1, y + 1, pochodzenie);
							}
								break;
							}
							zmeczony = 10;
						}
						if (wynik) delete wynik;
					}
				}

			}
		}
		break;
		}
 		if (zmeczony>0)
			zmeczony--;
		wiek++;
	}
	int Zwierze::kolizja(Zwierze *kolizujacy){
		if (kolizujacy->getId() == this->id){
			if (kolizujacy->zmeczony == 0 && this->zmeczony == 0){
				this->zmeczony = 6;
				return ROZMNAZAJ;
			}
			else return NIC;
		}
		else if (kolizujacy->getSila() >= this->sila){
			return ZABIJ;
		}
		else if (kolizujacy->getSila() < this->sila){
			return UMIERAJ;
		}
	}
	void Zwierze::addSila(int i){
		sila = sila + i;
	}
}