#pragma once
//using namespace std;
#include "Zolw.h"
#include "biblioteka.h"
namespace project{
	Zolw::Zolw(int xx, int yy, Swiat &swiat)
		:
		Zwierze(xx, yy, 204, 2, 1, "zolw", 1, swiat){};
	Zolw::~Zolw(){
		std::ostringstream inttostring;
		std::string komunikat;
		komunikat += nazwa;
		komunikat += " ginie na pozycji x ";
		inttostring << x + 1;
		komunikat += inttostring.str();
		inttostring.str("");
		komunikat += ", y ";
		inttostring << y + 1;
		komunikat += inttostring.str();
		inttostring.str("");
		pochodzenie.wypiszkomunikat(komunikat);
	};
	void Zolw::akcja(){
		int pom = rand() % 4;
		if (pom == 0) {
			Zwierze::akcja();
		}
		else {
			wiek++;
			if (zmeczony > 0)
				zmeczony--;
			return;
		}
	}
	int Zolw::kolizja(Zwierze *kolizujacy){
		if (kolizujacy->getId() == this->id){
			if (kolizujacy->getZmeczony() == 0 && this->zmeczony == 0){
				this->zmeczony = 6;
				return ROZMNAZAJ;
			}
			else return NIC;
		}
		else if (kolizujacy->getSila()-5 >= this->sila){
			return ZABIJ;
		}
		else if (kolizujacy->getSila() < this->sila){
			return UMIERAJ;
		}
		else return NIC;
	}
	char Zolw::rysowanie(){
		return 'T';
	}
}