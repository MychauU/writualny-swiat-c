#pragma once
//using namespace std;
#include "antylopa.h"
#include "biblioteka.h"
namespace project{
	Antylopa::Antylopa(int xx, int yy, Swiat &swiat)
		:
		Zwierze(xx, yy, 204, 4, 4, "antylopa", 1, swiat){};
	Antylopa::~Antylopa(){
		std::ostringstream inttostring;
		std::string komunikat;
		komunikat += nazwa;
		komunikat += " ginie na pozycji x ";
		inttostring << x + 1;
		komunikat += inttostring.str();
		inttostring.str("");
		komunikat += ", y ";
		inttostring << y + 1;
		komunikat += inttostring.str();
		inttostring.str("");
		pochodzenie.wypiszkomunikat(komunikat);
	};
	int Antylopa::kolizja(Zwierze *kolizujacy){
		int max_x = pochodzenie.getXsize();
		 int max_y = pochodzenie.getYsize();
		Organizm **organizmy = pochodzenie.getOrg();
		if (kolizujacy->getId() == this->id){
			if (kolizujacy->getZmeczony() == 0 && this->zmeczony == 0){
				this->zmeczony = 6;
				return ROZMNAZAJ;
			}
			else return NIC;
		}
		else if (kolizujacy->getSila() >= this->sila){
			int pom = rand() % 2;
			if (pom == 1) return ZABIJ;
			else {
				char *wynik = new char[9]{};
				int proba = 0;
				if (this->x > 0 && this->y > 0){
					if (organizmy[x - 1 + (y - 1)*max_x] == NULL){
						wynik[proba] = '1';
						proba++;
					}
				}
				if (this->y > 0){
					if ((organizmy[x + (y - 1)*max_x] == NULL)){
						wynik[proba] = '2';
						proba++;
					}
				}
				if (this->x < max_x - 1 && this->y > 0){
					if ((organizmy[x + 1 + (y - 1)*max_x] == NULL)){
						wynik[proba] = '3';
						proba++;
					}
				}
				if (this->x > 0){
					if ((organizmy[x - 1 + (y)*max_x] == NULL)){
						wynik[proba] = '4';
						proba++;
					}
				}
				if (this->x < max_x - 1){
					if ((organizmy[x + 1 + (y)*max_x] == NULL)){
						wynik[proba] = '5';
						proba++;
					}
				}
				if (this->x > 0 && this->y < max_y - 1){
					if ((organizmy[x - 1 + (y + 1)*max_x] == NULL)){
						wynik[proba] = '6';
						proba++;
					}
				}
				if (this->y < max_y - 1){
					if ((organizmy[x + (y + 1)*max_x] == NULL)){
						wynik[proba] = '7';
						proba++;
					}
				}
				if (this->x < max_x - 1 && this->y < max_y - 1){
					if ((organizmy[x +1+ (y + 1)*max_x] == NULL)){
						wynik[proba] = '8';
						proba++;
					}
				}
				if (proba == 0) wynik[0] = NULL;
				if (wynik[0] != NULL){
					for (proba = 1; wynik[proba] != NULL; proba++);
					proba = rand() % (proba);
					switch (wynik[proba]){
					case '1':{
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x -1+ (y - 1)*max_x]);
						this->y--;
						this->x--;
					}
						break;
					case '2':{
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y - 1)*max_x]);
						this->y--;
					}
						break;
					case '3':{
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x+1 + (y - 1)*max_x]);
						this->y--;
						this->x++;
					}
						break;
					case '4':{
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x -1+ (y)*max_x]);
						this->x--;
					}
						break;
					case '5':{
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + 1 + (y)*max_x]);
						this->x++;
					}
						break;
					case '6':{
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x - 1 + (y+1)*max_x]);
						this->x--;
						this->y++;
					}
						break;
					case '7':{
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x  + (y+1)*max_x]);
						this->y++;
					}
						break;
					case '8':{
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + 1 + (y+1)*max_x]);
						this->x++;
						this->y++;
					}
						break;
					}
					return UCIEKL;
				}
				else return ZABIJ;
			}
		}
		else if (kolizujacy->getSila() < this->sila){
			return UMIERAJ;
		}
	}
	void Antylopa::akcja(){
		int max_x = pochodzenie.getXsize();
		int max_y = pochodzenie.getYsize();
		Organizm **organizmy = pochodzenie.getOrg();
		Organizm **kolejnosc = pochodzenie.getKol();
		int size_active = pochodzenie.getSizeOfHeap();
		int ruch;
		int zdarzenie;
		ruch = rand() % 4;
		switch (ruch){
		case 0:{
			if (this->y > 1){
				if ((organizmy[x + (y - 2)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y - 2)*max_x]);
					this->y-=2;
				}
				else {
					zdarzenie = organizmy[x + (y - 2)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y - 2)*max_x]);
						this->y -= 2;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y - 2)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x + (y - 2)*max_x]);
						delete organizmy[x + (y - 2)*max_x];
						organizmy[x + (y - 2)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y - 2)*max_x]);
						this->y-=2;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y - 2)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x + (y - 2)*max_x]);
						delete organizmy[x + (y - 2)*max_x];
						organizmy[x + (y - 2)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);
					else if (zdarzenie == ROZMNAZAJ){
						char *wynik = new char[9]{};
						int proba = 0;
						if (this->x > 0 && this->y > 0){
							if (organizmy[x - 1 + (y - 1)*max_x] == NULL){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (this->y > 0){
							if ((organizmy[x + (y - 1)*max_x] == NULL)){
								wynik[proba] = '2';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y > 0){
							if ((organizmy[x + 1 + (y - 1)*max_x] == NULL)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = NULL;
						if (wynik[0] != NULL){
							for (proba = 1; wynik[proba] != NULL; proba++);
							proba = rand() % (proba);
							switch (wynik[proba]){
							case '1':{
								if (id == ANTYLOPA) organizmy[x - 1 + (y - 1)*max_x] = new  Antylopa(x - 1, y - 1, pochodzenie);
							}
								break;
							case '2':{
								if (id == ANTYLOPA) organizmy[x + (y - 1)*max_x] = new  Antylopa(x, y - 1, pochodzenie);
							}
								break;
							case '3':{
								if (id == ANTYLOPA) organizmy[x + 1 + (y - 1)*max_x] = new  Antylopa(x + 1, y - 1, pochodzenie);
							}
								break;
							}
							zmeczony = 10;
						}
						if (wynik) delete wynik;
					}
				}
			}
		}
			break;
		case 1:{
			if (this->y <max_y - 2){
				if ((organizmy[x + (y + 2)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y + 2)*max_x]);
					this->y+=2;
				}
				else {
					zdarzenie = organizmy[x + (y + 2)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y + 2)*max_x]);
						this->y+=2;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y +2)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
			//			pochodzenie.zmiennanull(organizmy[x + (y + 2)*max_x]);
						delete organizmy[x + (y + 2)*max_x];
						organizmy[x + (y + 2)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + (y + 2)*max_x]);
						this->y+=2;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x + (y +2)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x + (y + 2)*max_x]);
						delete organizmy[x + (y + 2)*max_x];
						organizmy[x + (y + 2)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);
					else if (zdarzenie == ROZMNAZAJ){
						char *wynik = new char[9]{};
						int proba = 0;
						if (this->y< max_y - 1){
							if ((organizmy[x + (y + 1)*max_x] == NULL)){
								wynik[proba] = '7';
								proba++;
							}
						}
						if (this->x > 0 && this->y < max_y - 1){
							if ((organizmy[x - 1 + (y + 1)*max_x] == NULL)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y < max_y - 1){
							if ((organizmy[x +1 +(y + 1)*max_x] == NULL)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = NULL;
						if (wynik[0] != NULL){
							for (proba = 1; wynik[proba] != NULL; proba++);
							proba = rand() % (proba);
							switch (wynik[proba]){
							case '6':{
								if (id == ANTYLOPA) organizmy[x - 1 + (y + 1)*max_x] = new  Antylopa(x - 1, y + 1, pochodzenie);
							}
								break;
							case '7':{
								if (id == ANTYLOPA) organizmy[x + (y + 1)*max_x] = new  Antylopa(x, y + 1, pochodzenie);
							}
								break;
							case '8':{
								if (id == ANTYLOPA) organizmy[x + 1 + (y + 1)*max_x] = new  Antylopa(x + 1, y + 1, pochodzenie);
							}
								break;
							}
							zmeczony = 10;
						}
						if (wynik) delete wynik;
					}
				}

			}
		}
			break;
		case 2:{
			if (this->x >1){
				if ((organizmy[x - 2 + (y)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x - 2 + (y)*max_x]);
					this->x-=2;
				}
				else {
					zdarzenie = organizmy[x - 2 + (y)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x - 2 + (y)*max_x]);
						this->x-=2;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x-2 + (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x - 2 + (y)*max_x]);
						delete organizmy[x - 2 + (y)*max_x];
						organizmy[x - 2 + (y)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x - 2 + (y)*max_x]);
						this->x-=2;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x -2+ (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x - 2 + (y)*max_x]);
						delete organizmy[x - 2 + (y)*max_x];
						organizmy[x - 2 + (y)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);
					else if (zdarzenie == ROZMNAZAJ){
						char *wynik = new char[9]{};
						int proba = 0;
						if (this->x > 0 && this->y > 0){
							if (organizmy[x - 1 + (y - 1)*max_x] == NULL){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (this->x > 0){
							if ((organizmy[x - 1 + (y)*max_x] == NULL)){
								wynik[proba] = '4';
								proba++;
							}
						}
						if (this->x > 0 && this->y < max_y - 1){
							if ((organizmy[x - 1 + (y + 1)*max_x] == NULL)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = NULL;
						if (wynik[0] != NULL){
							for (proba = 1; wynik[proba] != NULL; proba++);
							proba = rand() % (proba);
							switch (wynik[proba]){
							case '1':{
								if (id == ANTYLOPA) organizmy[x - 1 + (y - 1)*max_x] = new  Antylopa(x - 1, y - 1, pochodzenie);
							}
								break;
							case '4':{
								if (id == ANTYLOPA) organizmy[x - 1 + (y)*max_x] = new  Antylopa(x - 1, y, pochodzenie);
							}
								break;
							case '6':{
								if (id == ANTYLOPA) organizmy[x - 1 + (y + 1)*max_x] = new  Antylopa(x - 1, y + 1, pochodzenie);
							}
								break;
							}
							zmeczony = 10;
						}
						if (wynik) delete wynik;
					}
				}

			}
		}
			break;
		case 3:{
			if (this->x <max_x - 2){
				if ((organizmy[x + 2 + (y)*max_x] == NULL)){
					pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + 2 + (y)*max_x]);
					this->x+=2;
				}
				else {
					zdarzenie = organizmy[x + 2 + (y)*max_x]->kolizja(this);
					if (zdarzenie == UCIEKL){
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + 2 + (y)*max_x]);
						this->x+=2;
					}
					else if (zdarzenie == ZABIJ){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x +2+ (y )*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
					//	pochodzenie.zmiennanull(organizmy[x + 2 + (y)*max_x]);
						delete organizmy[x + 2 + (y)*max_x];
						organizmy[x + 2 + (y)*max_x] = NULL;
						pochodzenie.swappointers(&organizmy[x + (y)*max_x], &organizmy[x + 2 + (y)*max_x]);
						this->x+=2;
					}
					else if (zdarzenie == UMIERAJ){
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == ZUM){
						for (int i = 0; i < size_active; i++){
							if (organizmy[x +2+ (y)*max_x] == kolejnosc[i]){
								kolejnosc[i] = NULL;
								break;
							}
						}
				//		pochodzenie.zmiennanull(organizmy[x + 2 + (y)*max_x]);
						delete organizmy[x + 2 + (y)*max_x];
						organizmy[x + 2 + (y)*max_x] = NULL;
						organizmy[x + (y)*max_x] = NULL;
						delete this;
						return;
					}
					else if (zdarzenie == NIC);

					else if (zdarzenie == ROZMNAZAJ){
						char *wynik = new char[9]{};
						int proba = 0;
						if (this->x < max_x - 1 && this->y > 0){
							if ((organizmy[x + 1 + (y - 1)*max_x] == NULL)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (this->x < max_x - 1){
							if ((organizmy[x + 1 + (y)*max_x] == NULL)){
								wynik[proba] = '5';
								proba++;
							}
						}
						if (this->x < max_x - 1 && this->y < max_y - 1){
							if ((organizmy[x +1+ (y + 1)*max_x] == NULL)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = NULL;
						if (wynik[0] != NULL){
							for (proba = 1; wynik[proba] != NULL; proba++);
							proba = rand() % (proba);
							switch (wynik[proba]){
							case '3':{
								if (id == ANTYLOPA) organizmy[x + 1 + (y - 1)*max_x] = new  Antylopa(x + 1, y - 1, pochodzenie);
							}
								break;
							case '5':{
								if (id == ANTYLOPA) organizmy[x + 1 + (y)*max_x] = new  Antylopa(x + 1, y, pochodzenie);
							}
								break;
							case '8':{
								if (id == ANTYLOPA) organizmy[x + 1 + (y + 1)*max_x] = new  Antylopa(x + 1, y + 1, pochodzenie);
							}
								break;
							}
							zmeczony = 10;
						}
						if (wynik) delete wynik;
					}
				}

			}
		}
			break;
		}
		if (zmeczony>0)
			zmeczony--;
		wiek++;
	}
	char Antylopa::rysowanie(){
		return 'A';
	}
}