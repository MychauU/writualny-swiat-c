#pragma once
//using namespace std;
#include "Zwierze.h"
namespace project{
	class Czlowiek : public Zwierze{
		int moc;
		bool niesmiertelonsc;
		bool szybkosc;
		bool nietykalnosc;
		int powerbuff;
		int cooldown;
		void castSkill();
		void risePower();
	public:
		char rysowanie();
		void akcja();
		int kolizja(Zwierze* kolizujacy);
		Czlowiek(int xx, int yy, Swiat &swiat);
		~Czlowiek();
	};
}