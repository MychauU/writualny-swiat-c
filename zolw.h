#pragma once
//using namespace std;
#include "Zwierze.h"
namespace project{
	class Zolw : public Zwierze{
	public:
		char rysowanie();
		void akcja();
		int kolizja(Zwierze *kolizujacy);
		Zolw(int xx, int yy, Swiat &swiat);
		~Zolw();
	};
}